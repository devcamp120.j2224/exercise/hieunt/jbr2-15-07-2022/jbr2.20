public class Cylinder extends Circle {
    double height = 1.0;
    public Cylinder() {
        super();
    }
    public Cylinder(double radius) {
        super(radius);
    }
    public Cylinder(double radius,
    double height) {
        super(radius);
        this.height = height;
    }
    public Cylinder(double radius,
    double height, String color) {
        super(radius, color);
        this.height = height;
    }
    // getter setter
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public double getVolume() {
        double volume = this.getArea() * height;
        return volume;
    }
    @Override
    public String toString() {
        return "Cylinder [" + 
        super.toString()
        + ", height=" + height + "]";
    }
}
